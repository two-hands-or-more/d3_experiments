
// Factor that should be applied to the size of the circles.
const SCALE = 10;


/**
 * Displays the data circles.
 * 
 * `values` is the array of data that should be displayed.
 */
function plotCircles(values) {
  // Find the existing SVG display object
  const display = d3.select("#display");
  // Create an SVG group where we'll display the circles.
  const circleGroup = display.append("g")
    .attr("transform", "translate(10, 30)")
  ;

  // Attach data
  const circles = circleGroup
    .selectAll("circle")
    .data(values)
  ;

  // Find the maximum values so we can scale everything according to that.
  const maxValue = Math.max(...values);
  
  // Render circles
  const newNodes = circles.enter()
    .append("g")
    .attr("transform", (_, i) => `translate(${i * 50}, 10)`)
  ;
  // Add the circle objects
  const nodeCircles = newNodes.append("circle")
    .attr("r", (d) => (d / maxValue) * SCALE)
  ;
  // Add the popover information blurbs.
  const popover = newNodes.append("foreignObject")
    .attr("x", 0)
    .attr("y", 0)
    .attr("width", 100)
    .attr("height", 50)
    .append("xhtml:div")
      .attr("class", "popover-hidden")
      .text((d) => `Value: ${d}`)
  ;

  // Attach mouse-over event listeners
  nodeCircles
    .on("mouseover", (event) => d3.select(event.currentTarget.parentNode).select("div").attr("class", "popover-visible"))
    .on("mouseout", () => d3.select(event.currentTarget.parentNode).select("div").attr("class", "popover-hidden"))
  ;
}


plotCircles([4, 2, 12, 13, 5, 6]);
