
// Factor that should be applied to the size of the circles.
const SCALE = 10;


/**
 * Displays the data circles.
 * 
 * `values` is the array of data that should be displayed.
 */
function plotCircles(values) {
  // Find the existing SVG display object
  const display = d3.select("#display");
  // Create an SVG group where we'll display the circles.
  const circleGroup = display.append("g")
    .attr("transform", "translate(10, 30)");

  // Attach data
  const circles = circleGroup
    .selectAll("circle")
    .data(values);

  // Find the maximum values so we can scale everything according to that.
  const maxValue = Math.max(...values);
  
  // Render circles
  circles.enter()
    .append("circle")
    .attr("r", (d) => (d / maxValue) * SCALE)
    .attr("cx", (_, i) => i * 50);
}


plotCircles([4, 2, 12, 13, 5, 6]);
